-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2019 at 08:40 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keuangan`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_alat`
--

CREATE TABLE `data_alat` (
  `id` int(100) NOT NULL,
  `id_department` int(100) DEFAULT NULL,
  `id_jenisuser` int(100) DEFAULT NULL COMMENT 'as operator',
  `id_distribution` int(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `kondisi` int(1) DEFAULT NULL,
  `kks` varchar(100) DEFAULT NULL,
  `date_operation` datetime NOT NULL,
  `hour_operation` time NOT NULL,
  `financial` int(100) DEFAULT NULL,
  `operation` int(100) DEFAULT NULL,
  `environment` int(100) DEFAULT NULL,
  `safety` int(100) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_alat`
--

INSERT INTO `data_alat` (`id`, `id_department`, `id_jenisuser`, `id_distribution`, `name`, `kondisi`, `kks`, `date_operation`, `hour_operation`, `financial`, `operation`, `environment`, `safety`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 3, 1, 1, 'ignition GT 13', 1, '13MBM31', '2019-01-27 00:00:00', '691:10:40', 10, 20, 30, 12, '2019-01-27 02:01:42', '0000-00-00 00:00:00', NULL, 1),
(2, 3, 1, 1, 'temperature after turbin GT13', 1, '13MBA30CT', '2019-01-27 00:00:00', '838:59:59', 1, 2, 3, 4, '2019-01-27 16:01:13', '2019-01-27 16:01:19', NULL, 1),
(3, 4, 1, 2, 'Contorl Gas Valve GT 500', 2, '23MBP', '2019-01-28 00:00:00', '00:05:08', 100, 200, 300, 400, '2019-01-27 16:01:35', '2019-01-27 16:01:56', NULL, 1),
(4, 3, 1, 3, 'pulsation', 2, '13MBM30AX', '2019-01-27 00:00:00', '838:59:59', 7, 8, 9, 19, '2019-01-27 16:01:37', '2019-01-27 16:01:26', NULL, 1),
(5, 5, 1, 3, 'flame monitor', 3, '22MBM30', '2019-01-27 00:00:00', '591:05:08', 11, 24, 24, 45, '2019-01-27 16:01:46', '2019-01-27 16:01:40', NULL, 1),
(6, 5, 1, 3, 'gas controle valve GT 23', 3, '21MBP', '2019-01-27 00:00:00', '00:00:00', 90, 13, 23, 43, '2019-01-27 16:01:02', '0000-00-00 00:00:00', NULL, 1),
(7, 2, 1, 2, 'gas detection MP 1000', 2, '11cyq', '2019-01-27 00:00:00', '01:02:03', 20, 30, 30, 40, '2019-01-27 16:01:25', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_panjar`
--

CREATE TABLE `data_panjar` (
  `id` int(100) NOT NULL,
  `id_kegiatan` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_panjar`
--

INSERT INTO `data_panjar` (`id`, `id_kegiatan`, `name`, `tanggal`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 1, 'kegiatan 1', '2019-02-14 00:00:00', '2019-02-28 15:02:45', '0000-00-00 00:00:00', NULL, 0),
(2, 2, 'kegiatan 2', '2019-02-20 00:00:00', '2019-02-28 15:02:28', '0000-00-00 00:00:00', NULL, 1),
(3, 1, 'Kegiatan 3', '2019-02-28 00:00:00', '2019-02-28 15:02:15', '2019-02-28 15:02:00', NULL, 1),
(4, 1, 'Kegiatan 4', '2019-02-27 00:00:00', '2019-02-28 15:02:36', '2019-02-28 16:02:13', NULL, 1),
(5, 1, 'Kegiatan 5', '2019-03-21 00:00:00', '2019-02-28 15:02:43', '2019-02-28 16:02:28', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_panjar_rekening`
--

CREATE TABLE `data_panjar_rekening` (
  `id` int(100) NOT NULL,
  `id_kegiatan` varchar(100) NOT NULL,
  `id_rekening` varchar(100) NOT NULL,
  `nominal` int(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_panjar_rekening`
--

INSERT INTO `data_panjar_rekening` (`id`, `id_kegiatan`, `id_rekening`, `nominal`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, '1', '1', 10, '2019-02-28 16:02:21', '0000-00-00 00:00:00', NULL, 1),
(2, '1', '2', 20, '2019-02-28 16:02:34', '2019-02-28 17:02:12', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_panjar_rekening_realisasi`
--

CREATE TABLE `data_panjar_rekening_realisasi` (
  `id` int(100) NOT NULL,
  `id_rekening` varchar(100) NOT NULL,
  `id_transaksi` varchar(100) DEFAULT '0',
  `id_pekerjaan` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `nominal` int(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_panjar_rekening_realisasi`
--

INSERT INTO `data_panjar_rekening_realisasi` (`id`, `id_rekening`, `id_transaksi`, `id_pekerjaan`, `pekerjaan`, `keterangan`, `nominal`, `tanggal`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, '1', '1802210', '12102181', 'JK JKK e dcd', 'JK JKK Deskripsi wfv', 1, '2019-03-03 00:00:00', '0000-00-00 00:00:00', '2019-02-28 18:02:26', NULL, 1),
(2, '2', '180221', '210218', 'konseling mooping', 'keterangan\r\nketerangan\r\nkletergrwg', 2, '2019-03-29 00:00:00', '2019-02-28 18:02:21', '0000-00-00 00:00:00', NULL, 1),
(3, '1', '180229', '290218', 'konseling mooping 2', 'frwogkrwog wrpvgrwp\r\nrwo\r\nrwog', 3, '2019-02-28 00:00:00', '2019-02-28 18:02:29', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_user`
--

CREATE TABLE `data_user` (
  `id` int(100) NOT NULL,
  `id_department` int(100) NOT NULL,
  `id_role` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `nik` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_user`
--

INSERT INTO `data_user` (`id`, `id_department`, `id_role`, `name`, `nik`, `email`, `password`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 4, 2, 'hisyaman', '102030405064', 'hisyaman@mail.com', 'hisyaman', '2019-01-26 04:01:21', '2019-01-26 04:01:13', NULL, 1),
(2, 2, 3, 'alqa', '1020304050601', 'alqa@mail.com', 'alqa', '2019-01-26 04:01:14', '0000-00-00 00:00:00', NULL, 1),
(3, 5, 1, 'administrasi', '102030406070', 'administrasi@mail.com', 'administrasi', '2019-01-26 04:01:51', '0000-00-00 00:00:00', NULL, 1),
(4, 6, 3, 'tito birawa', '1020304050602', 'titobirawa@mail.com', 'titobirawa', '2019-01-26 04:01:38', '0000-00-00 00:00:00', NULL, 1),
(5, 3, 3, 'vincent', '1020304050603', 'vincent@mail.com', 'vincent', '2019-01-26 04:01:08', '0000-00-00 00:00:00', NULL, 1),
(6, 1, 3, 'doniherma', '1020304050609', 'doniherma@mail.com', 'doniherma', '2019-01-26 19:01:12', '0000-00-00 00:00:00', NULL, 1),
(7, 1, 3, 'ekosantoso', '1020304050670', 'ekosantoso@mail.com', 'ekosantoso', '2019-01-26 19:01:39', '0000-00-00 00:00:00', NULL, 1),
(8, 2, 2, 'rijal', '1094042403320', 'rijal@mail.com', 'rijal', '2019-01-27 20:01:11', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_department`
--

CREATE TABLE `m_department` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_department`
--

INSERT INTO `m_department` (`id`, `name`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 'PP', '2019-01-26 04:01:00', '2019-02-28 13:02:43', NULL, 1),
(2, 'puha', '2019-01-26 04:01:08', '2019-02-28 13:02:50', NULL, 1),
(3, 'dalduk', '2019-01-26 04:01:26', '2019-02-28 13:02:02', NULL, 1),
(4, 'kk', '2019-01-26 04:01:40', '2019-02-28 13:02:14', NULL, 1),
(5, 'tu', '2019-01-26 04:01:48', '2019-02-28 13:02:20', NULL, 1),
(6, 'ST14EDIT', '2019-01-26 04:01:57', '2019-02-28 13:02:07', NULL, 1),
(7, 'ST14', '2019-01-27 20:01:28', '2019-02-28 13:02:33', NULL, 1),
(8, 'BIDANG A', '2019-02-28 13:02:21', '0000-00-00 00:00:00', NULL, 1),
(9, 'BIDANG B', '2019-02-28 13:02:33', '0000-00-00 00:00:00', NULL, 1),
(10, 'bidang c', '2019-02-28 13:02:43', '0000-00-00 00:00:00', NULL, 1),
(11, 'bidang d', '2019-02-28 13:02:49', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_kegiatan`
--

CREATE TABLE `m_kegiatan` (
  `id` int(100) NOT NULL,
  `id_department` int(100) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kegiatan`
--

INSERT INTO `m_kegiatan` (`id`, `id_department`, `kode`, `description`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 1, '112.0201.0001', 'Penguatan dan pengendalian jaringan pengarusuhan gender.', '2019-02-28 13:02:03', '2019-02-28 13:02:05', NULL, 1),
(2, 2, '112.0202.0002', 'Pembinaaan dan pendampingan masalah perempuan dan anak', '2019-02-28 13:02:58', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_rekening`
--

CREATE TABLE `m_rekening` (
  `id` int(100) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_rekening`
--

INSERT INTO `m_rekening` (`id`, `kode`, `description`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, '5.2.2.03.01', 'Beli Air', '2019-02-28 13:02:59', '2019-02-28 13:02:40', NULL, 1),
(2, '5.2.2.03.02', 'Beli Telepon\r\n', '2019-02-28 13:02:08', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_role`
--

CREATE TABLE `m_role` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(100) DEFAULT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_role`
--

INSERT INTO `m_role` (`id`, `name`, `date_created`, `date_updated`, `created_by`, `is_active`) VALUES
(1, 'Administrasi', '2019-01-26 04:01:37', '0000-00-00 00:00:00', NULL, 1),
(2, 'Kepala Bidang', '2019-01-26 04:01:44', '2019-02-28 14:02:33', NULL, 1),
(3, 'Staff Bidang', '2019-01-26 04:01:54', '2019-02-28 14:02:44', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_alat`
--
ALTER TABLE `data_alat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_panjar`
--
ALTER TABLE `data_panjar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_panjar_rekening`
--
ALTER TABLE `data_panjar_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_panjar_rekening_realisasi`
--
ALTER TABLE `data_panjar_rekening_realisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_user`
--
ALTER TABLE `data_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_department`
--
ALTER TABLE `m_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_rekening`
--
ALTER TABLE `m_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_role`
--
ALTER TABLE `m_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_alat`
--
ALTER TABLE `data_alat`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_panjar`
--
ALTER TABLE `data_panjar`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `data_panjar_rekening`
--
ALTER TABLE `data_panjar_rekening`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_panjar_rekening_realisasi`
--
ALTER TABLE `data_panjar_rekening_realisasi`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_user`
--
ALTER TABLE `data_user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_department`
--
ALTER TABLE `m_department`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_rekening`
--
ALTER TABLE `m_rekening`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_role`
--
ALTER TABLE `m_role`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
