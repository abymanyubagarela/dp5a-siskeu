<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panjar extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 


		## load model here 
		$this->load->model('PanjarModel', 'Panjar');
		$this->load->model('PanjarRekeningModel', 'PanjarRekening');
		$this->load->model('DetailRekeningModel', 'DetailRekening');
		$this->load->model('KegiatanModel', 'Kegiatan');
		$this->load->model('RekeningModel', 'Rekening');

		$this->data = array(
            'controller'	=> 'panjar',
            'title' 		=> 'Panjar',
            'list'			=>  $this->Panjar->getAll(),
            'kegiatan'		=>  $this->Kegiatan->getAll() ,
            'rekening'		=>  $this->Rekening->getAll()      
        );
	}

	public function index()	{
		
		$data = $this->data;
		if (!empty($_POST)) {
			$data['filter']['bulan'] = $_POST['bulan'];
			$data['filter']['tahun'] = $_POST['tahun'];
			$data['list'] = $this->Panjar->getAllbydate($data['filter']['tahun'],$data['filter']['bulan']) ;
		}  else {
			$data['filter']['bulan'] = date('n');
			$data['filter']['tahun'] = date('Y');
			$data['list'] = $this->Panjar->getAllbydate(date('Y'),date('M')) ;
		}
		
		foreach ($data['list'] as $key => $p) {
			$p->total = $this->Panjar->sumPanjar($p->id);
		}

		$this->load->view('inc/panjar/list', $data);
	}

	public function safe() {	

		$data = $this->data;

		$this->load->view('inc/panjar/list', $data);
	}

	public function create()	{	

		$data = $this->data;

		$data['title'] = 'Input Panjar' ;

		$this->load->view('inc/panjar/create', $data);
	}

	public function insert() {

		$exist = $this->Panjar->isKegiatanExist($this->input->post('id_kegiatan'));
		if(empty($exist)) {
			$err = $this->Panjar->insert();
			if ($err['code'] == '0') {			
				$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
			} else {
				$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
			}
		} else {
			$this->session->set_flashdata('failed', 'Gagal Data Kode Kegiatan Sudah Ada');
		}

		redirect($this->data['controller']);
	}

	public function edit($id) {

		$data = $this->data;

		$data['list_edit'] = $this->Panjar->getByID($id) ;

		$data['list_edit']->tanggal = date('Y-m-d',strtotime($data['list_edit']->tanggal));

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));
	}

	public function update() {

		$err = $this->Panjar->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		

		redirect($this->data['controller']);
	}

	public function delete($id) {

		$err = $this->Panjar->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function detail($id) {

		$data = $this->data;

	    $data['title'] = 'Rekening Kegiatan Per Panjar' ;

		$data['list_edit'] = $this->Panjar->getByID($id) ;

		$data['list'] = $this->PanjarRekening->getAll($id) ;

		$this->load->view('inc/panjar/detail', $data);
	}

	public function insertdetail() {
		
		$id_kegiatan = $_POST['id_kegiatan'];

		$err = $this->PanjarRekening->insert();

		if ($err['code'] == '0') {			
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller'].'/detail/'.$id_kegiatan);
	}

	public function editdetail($id) {

		$data = $this->data;

		$data['list_edit'] = $this->PanjarRekening->getByID($id) ;

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));
	}

	public function updatedetail() {

		$id_kegiatan = $_POST['id_kegiatan'];

		$err = $this->PanjarRekening->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller'].'/detail/'.$id_kegiatan);
	}

	public function deletedetail($id) {

		$err = $this->PanjarRekening->delete($id);
		
		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($err));
	}

	public function laporan($id) {

		$data = $this->data;
		$data['list_edit'] = $this->Panjar->getByID($id) ;
		$data['rekening'] = $this->PanjarRekening->getAllByID($id) ;
		$data['totalpanjar'] = 0 ;
		$panjar = 0 ;
		
		foreach ($data['rekening'] as $key => $r) {
			$panjar += $r->nominal ;

			##get total##
			$uraian = $this->DetailRekening->getAllByID($r->id_kegiatan,$r->id_rekening) ;
			
			$realisasitotal = 0 ;
			foreach ($uraian as $key => $u) {
				$realisasitotal += $u->nominal ;
			}
			##get total##

			$r->uraian = $uraian ;
 			$r->realisasitotal  = $realisasitotal ;
 			$r->panjar = $panjar;
 			$r->sisa = $r->nominal - $r->realisasitotal ;
 			// $r->x = $r->panjar ;
 			// $r->y = $r->realisasitotal ;
		}
		//print_r($data['rekening']);
		$data['totalpanjar'] = $panjar ;

		$this->load->view('inc/panjar/laporan', $data);
	}
}
