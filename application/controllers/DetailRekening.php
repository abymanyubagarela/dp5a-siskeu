<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailRekening extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();	
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		## load model here 
		$this->load->model('DetailRekeningModel', 'DetailRekening');
		$this->load->model('KegiatanModel', 'Kegiatan');
		$this->load->model('RekeningModel', 'Rekening');
		$this->load->model('PanjarModel', 'Panjar');

		$this->data = array(
            'controller'	=> 'detailrekening',
            'title' 		=> 'DetailRekening',
            'kegiatan'		=> $this->Kegiatan->getAll() ,
            'rekening'		=> $this->Rekening->getAll() ,
            'pph21'			=> array( '5' => '5%',  '6' => '6%', '15' => '15%') ,     
            'pph22'			=> array( '1,5' => '1,5%',  '3' => '3%'),
            'pph23'			=> array( '2' => '2%',  '4' => '4% (Mamin)',  '5' => '4% (Jasa)'),     
            'ppn'			=> array( '10' => '10%'),     
            'sspd'			=> array( '10' => '10%'),
            'final'			=> array( '0.5' => '0.5%','10' => '10%'),     
        );
	}

	public function index() {
		
		$data = $this->data;
		
		$this->load->view('inc/detailrekening/list', $data);
	}

	public function detail($id) {
		$_temp = explode('-',$id);
			
		$data = $this->data;

	    $data['title'] = 'Realisasi Kegiatan Per Detail Rekening' ;

	    $data['detail'] = $this->DetailRekening->getDetail($_temp[0]) ;
	    $data['department'] = $this->DetailRekening->getDepartment($_temp[0]) ;
	   	$data['rekening'] = $this->Rekening->getByID($_temp[1]) ;
	    $data['list']   = $this->DetailRekening->getAll($_temp);		
	    $data['list_edit'] = $this->Panjar->getByID($_temp[0]) ;

	    $data['id']     = $_temp[0] ;
	    $data['id_sub']     = $_temp[1] ;

	    $this->load->view('inc/panjardetailrekening/list', $data);
	}

	public function insert($id) {
		$err = $this->DetailRekening->insert();

		if ($err['code'] == '0') {			
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller'].'/detail/'.$_POST['id_parent'].'-'.$id);
	}

	public function edit($id) {

		$data = $this->data;

		$data['detail'] = $this->DetailRekening->getByID($id) ;

		$data['detail']->tanggal = date('Y-m-d',strtotime($data['detail']->tanggal));

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));
	}

	public function update($id) {
		
		$err = $this->DetailRekening->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	
		
		redirect($this->data['controller'].'/detail/'.$_POST['id_parent'].'-'.$id);
	}

	public function delete($id) {

		$err = $this->DetailRekening->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($err));
	}

	public function checktransaksi() {
		$id_transaksi = $_POST['id_transaksi'] ;

		$data['detail'] = $this->DetailRekening->getByIDTransaksi($id_transaksi);

		$this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));
	}
}
