<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();

		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		$this->load->model('PanjarModel', 'Panjar');
		$this->load->model('DepartmentModel', 'Department');

		$this->data = array(
            'controller'=>'welcome'
        );
	}

	public function index()	{	
		$data = $this->data;
		$data['department'] = $this->Department->getList();

		foreach ($data['department'] as $key => $d) {
			$_isfilled = $this->Panjar->getDashboard($d->id);

			$data['department'][$key]->isfilled = 0 ;
			if (!empty($_isfilled)) {
				$data['department'][$key]->isfilled = 1 ;
			}
		}

		$this->load->view('inc/dashboard',$data);		
	}	
}
