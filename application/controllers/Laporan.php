<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
            'controller'=>'laporan',
            'rank'=> array('1' => 'Low', '2' => 'Intermediate' , '3' => 'High' ),
            'status'=> array('1' => 'Open', '2' => 'Closed')
        );

		## load model here 
		$this->load->model('PanjarModel', 'Panjar');
		$this->load->model('PanjarRekeningModel', 'PanjarRekening');
	}

	public function pekerjaan() {	
		$data = $this->data;
	}

	public function pengajuan() {	

		$data = $this->data;
		$data['title'] = 'Laporan Pengajuan Panjar' ;

		if (!empty($_POST)) {
			$data['filter']['bulan'] = $_POST['bulan'];
			$data['filter']['tahun'] = $_POST['tahun'];

			$data['pengajuan'] = $this->Panjar->getListPengajuan($data['filter']['tahun'],$data['filter']['bulan']) ;
		}

		if (!empty($data['pengajuan'])) {
			foreach ($data['pengajuan'] as $key => $r) {
				$r->detail = $this->PanjarRekening->getPengajuanByID($r->id,$data['filter']['bulan'],$data['filter']['tahun']);
			}	
		}
		$this->load->view('inc/laporan/pengajuan', $data);
	}
}
