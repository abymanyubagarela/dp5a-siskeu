<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman Data <?= $title ?></h5>                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>                                      
                                        <th width="8%">Raised</th>
                                        <th width="8%">Finish</th>
                                        <th>KKS</th>
                                        <th>Unit</th>
                                        <th>Pekerjaan</th>
                                        <th>Critical Rank</th>
                                        <th>User</th>
                                        <th>PIC</th>                                        
                                        <th>Durasi</th>
                                        <th>Status</th>                                        
                                        <th>Action</th>
                                        <th>Hasil</th>                                        
                                        <th>Temuan</th>
                                        <th>Keterangan</th>
                                        <th>Source</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= date('d M Y',strtotime($row->date_raised)) ?></td>
                                            <td><?= date('d M Y',strtotime($row->date_finished)) ?></td>                                         
                                            <td><?= $row->kks ?></td>
                                            <td><?= $row->id_department ?></td>
                                            <td><?= ucwords($row->name) ?></td>
                                            <td>
                                                <?php if ($row->rank == '1'): ?>
                                                    <div class="badge badge-success"><?= $rank[$row->rank] ?></div>
                                                <?php elseif($row->rank == '2'): ?>
                                                    <div class="badge badge-warning"> <?= $rank[$row->rank] ?></div>
                                                <?php else: ?>
                                                    <div class="badge badge-danger"> <?= $rank[$row->rank] ?></div>
                                                <?php endif ?>
                                            </td>
                                            <td><?= ucwords($row->id_user) ?></td>
                                            <td>
                                                <?php foreach ($row->id_pic as $key => $pic): ?>
                                                    <?= $key+1 .'.'.ucwords($pic->name) ?> <br>
                                                <?php endforeach ?>
                                            </td>                                            
                                            <td><?= $row->durasi ?> Jam</td>
                                            <td> <b> <u><?= $status[$row->status] ?> </u></b> </td>   
                                            <td>
                                                <?php foreach ($row->tindakan as $key => $t): ?>
                                                    <?php if ($t != null): ?>
                                                        <?= $key+1 .'.'.ucwords($t) ?> <br>    
                                                    <?php endif ?>                                                    
                                                <?php endforeach ?>
                                            </td>
                                            <td>
                                                <?php foreach ($row->hasil as $key => $h): ?>
                                                    <?php if ($h != null): ?>
                                                        <?= $key+1 .'.'.ucwords($h) ?> <br>    
                                                    <?php endif ?>                                                    
                                                <?php endforeach ?>
                                            </td>
                                            <td>
                                                <?php foreach ($row->temuan as $key => $te): ?>
                                                    <?php if ($te != null): ?>
                                                        <?= $key+1 .'.'.ucwords($te) ?> <br>    
                                                    <?php endif ?>                                                    
                                                <?php endforeach ?>
                                            </td>
                                            <td>
                                                <?php foreach ($row->keterangan as $key => $k): ?>
                                                    <?php if ($k != null): ?>
                                                        <?= $key+1 .'.'.ucwords($k) ?> <br>    
                                                    <?php endif ?>                                                    
                                                <?php endforeach ?>
                                            </td>
                                            <td><?= ucwords($row->id_source) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
    </body>
</html>