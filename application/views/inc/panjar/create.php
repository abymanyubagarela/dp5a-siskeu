<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman <?= $title ?></h5>
                            <br>
                            
                            <?php echo form_open_multipart($controller.'/insert'); ?>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Nama Alat</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Kondisi</label>
                                            <select class="form-control" name="kondisi">
                                                <?php foreach ($kondisi as $key => $k): ?>
                                                    <option value="<?= $key ?>" > <?= $k ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">KKS</label>
                                            <input type="text" class="form-control" name="kks">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Unit</label>
                                            <select class="form-control" name="id_department">
                                                <?php foreach ($department as $key => $d): ?>
                                                    <option value="<?= $d->id ?>" > <?= $d->name ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">User</label>
                                            <select class="form-control" name="id_jenisuser">
                                                <?php foreach ($jenisuser as $key => $j): ?>
                                                    <option value="<?= $j->id ?>" > <?= $j->name ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Distribution</label>
                                            <select class="form-control" name="id_distribution">
                                                <?php foreach ($distribution as $key => $di): ?>
                                                    <option value="<?= $di->id ?>" > <?= ucwords($di->name) ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Tanggal Operasi</label>
                                            <input type="date" class="form-control" name="date_operation">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Jam Operasi</label>
                                            <input type="text" class="form-control" name="hour_operation" placeholder="120:53:23">
                                            <small>pisahkan dengan <b><u>: (titik 2)</u></b> , untuk membedakan jam:menit:detik</small>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Financial</label>
                                            <input type="number" class="form-control" name="financial">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Operasi</label>
                                            <input type="number" class="form-control" name="operation">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Lingkungan</label>
                                            <input type="number" class="form-control" name="environment">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Safety</label>
                                            <input type="number" class="form-control" name="safety">
                                        </div>
                                    </div>
                                </fieldset>

                                <div>
                                    <br>
                                    <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

    </body>
</html>