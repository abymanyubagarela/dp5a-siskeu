<!DOCTYPE html>

<html>
    <head>
        <?php $this->load->view('layout/header') ?>

        <style type="text/css">
            @media print {
                .print {
                    visibility: hidden;
                }

                .wrapper {
                    padding-top: 10px;
                }

                .table {
                    font-size: 8px!important;
                }

                .id {
                    max-width: 65px;
                }

                .table > thead > tr > th {
                    padding: 12px 5px!important;
                }

                @page {
                    size: landscape; 
                    margin: none;  
                }
            }

            .table thead th {
                vertical-align: middle;
                text-align: center;
            }

            .table {
                font-size: 10px;
            }

            .mark {
                font-size: 13px;
            }

            .parent {
                font-weight: bold;
            }

        </style>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row print">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <button type="button" class="btn btn-success waves-light waves-effect w-md print preview">Cetak</button> 

                            <table class="table mark" cellspacing="0" width="100%">
                                <tr>
                                    <td width="5%">Kegiatan</td>
                                    <th width="30%"><u><?= ucwords($list_edit->description) ?></u> </th>
                                    <td width="5%">Total Panjar</td>
                                    <th width="30%"><b>Rp. <?= number_format($totalpanjar) ?>,00 -</b></th>
                                </tr>

                                <tr>
                                    <td width="5%">Kode Kegiatan</td>
                                    <th width="30%"><u><?= ucfirst($list_edit->kode) ?></u> </th> 
                                    <td width="5%">Bulan</td>
                                    <th width="30%"><u><?= strtoupper(date('M',strtotime($list_edit->tanggal))) ?></u></th>
                                </tr>
                                <tr>
                                    <td width="5%">Jenis Kas</td>
                                    <th width="30%"><u><?= $list_edit->jenis == '1' ? 'GU' : 'TU' ?></u></th>
                                </tr>
                            </table>    
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">    
                            <table class="table table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="3">No</th>
                                        <th rowspan="3">Kode <br>Rek</th>
                                        <th rowspan="3" width="30%">Uraian</th>
                                        <th rowspan="3">Panjar</th>
                                        <th rowspan="3">Realisasi</th>
                                        <th rowspan="3">Sisa <br>Panjar</th>
                                        <th rowspan="3" class="id">ID Pekerjaan</th>
                                        <!-- <th rowspan="3" class="id">ID Transaksi</th> -->
                                        <!-- <th rowspan="3">Tgl <br>Pajak</th> -->
                                        <th rowspan="3">NTB</th>
                                        <th rowspan="3">NTPN</th>
                                        <th colspan="12">Pajak</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">PPH 21</th>
                                        <th colspan="2">PPH 22</th>
                                        <th colspan="2">PPH 23</th>
                                        <th>PPN</th>
                                        <th colspan="2">Final</th>
                                        <th>SSPD</th>
                                    </tr>
                                    <tr>
                                        <th>5%</th>
                                        <th>6%</th>
                                        <th>15%</th>
                                        <th>C</th>
                                        <th>1.5%</th>
                                        <th>3%</th>
                                        <th>2%</th>
                                        <th>4%</th>
                                        <th>10%</th>
                                        <th>0.5%</th>
                                        <th>10%</th>
                                        <th>10%</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    $total = 0 ;
                                    $totalrek = 0 ;
                                    $pph215 = 0 ; 
                                    $pph216 = 0 ; 
                                    $pph217 = 0 ; 
                                    $pph2178 = 0 ; 
                                    $pph221 = 0 ; 
                                    $pph223 = 0 ; 
                                    $pph232 = 0 ; 
                                    $pph234 = 0 ; 
                                    $ppn = 0 ; 
                                    $sspd = 0 ; 
                                    $final = 0 ; 
                                    $final05 = 0 ; 

                                    foreach ($rekening as $key => $r) { ?>
                                        <tr class="parent">
                                            <td><?= $i++ ?></td>
                                            <td><?= strtoupper($r->koderekening) ?></td>
                                            <td><?= ucwords($r->descriptionrekening) ?></td>
                                            <td><?= number_format($r->nominal) ?></td>
                                            <td> *</td>
                                            <td><?= number_format($r->sisa) ?></td>

                                            <?php for ($j=0; $j <= 14; $j++) { ?>
                                            <td> *</td>
                                            <?php } ?>
                                        </tr>

                                        <?php $total += $r->nominal ?>
                                        <?php if (!empty($r->uraian)): ?>
                                            <?php foreach ($r->uraian as $key => $u): ?>
                                                <tr class="child">
                                                    <td></td>
                                                    <td><?= date('d/m/Y',strtotime($u->tanggal)) ?></td>
                                                    <td><?= ucwords($u->pekerjaan) ?></td>
                                                    <td>*</td>
                                                    <td><?= number_format($u->nominal) ?></td>
                                                    <td>*</td>
                                                    <td class="id"><?= $u->id_pekerjaan ?></td>
                                                    <!-- <td class="id"><?= $u->id_transaksi ?></td> -->
                                                    <!-- <td>
                                                    <?php   
                                                        if ($u->tanggal_pajak == '0000-00-00') {
                                                            echo "*";
                                                        } else {
                                                            echo date('d/m/Y',strtotime($u->tanggal_pajak)) ;
                                                        }
                                                    ?>
                                                    </td> -->
                                                    <td><?= $u->ntb ?></td>
                                                    <td><?= strtoupper($u->ntpn) ?></td>
                                                    <!-- pajak -->
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph21 == '5') {
                                                                echo number_format((5/100) * $u->nominal,2);
                                                                $pph215 += (5/100) * $u->nominal ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph21 == '6') {
                                                                echo number_format((6/100) * $u->nominal ,2);
                                                                $pph216 += (6/100) * $u->nominal;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph21 == '15') {
                                                                echo number_format((15/100) * $u->nominal, 2);
                                                                $pph217 += (15/100) * $u->nominal ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph21 != '15' && $u->pph21 != '6' && $u->pph21 != '5') {
                                                                echo number_format($u->pph21 , 2);
                                                                $pph2178 +=  $u->pph21 ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>

                                                    <td>  
                                                        <?php  
                                                            if ($u->pph22 == '1,5') {
                                                                echo number_format(round((1.5/100) * $u->nominal / 1.1) , 2);
                                                                $pph221 += round((1.5/100) * $u->nominal / 1.1) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph22 == '3') {
                                                                echo number_format(round((3/100) * $u->nominal) , 2);
                                                                $pph223 += round((3/100) * $u->nominal) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph23 == '2') {
                                                                echo number_format(round((2/100) * $u->nominal / 1.1) , 2);
                                                                $pph232 += round((2/100) * $u->nominal / 1.1) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->pph23 == '4') {
                                                                echo number_format(round((4/100) * $u->nominal / 1.1),2);
                                                                $pph234 += round((4/100) * $u->nominal / 1.1) ;
                                                            } else if ($u->pph23 == '5') {
                                                                echo number_format(round((4/100) * $u->nominal),2);
                                                                $pph234 += round((4/100) * $u->nominal) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->ppn == '10') {
                                                                echo number_format(round($u->nominal / 11) , 2);
                                                                $ppn += round($u->nominal / 11) ; 
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>  
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->final == '0.5') {
                                                                echo number_format(round((5/1000) * $u->nominal / 1.1),2);
                                                                $final05 += round((5/1000) * $u->nominal / 1.1) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->final == '10') {
                                                                echo number_format(round($u->nominal / 11 ));
                                                                $final += round($u->nominal / 11) ; 
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php  
                                                            if ($u->sspd == '10') {
                                                                echo number_format(round($u->nominal / 11));
                                                                $sspd += round($u->nominal / 11) ;
                                                            } else {
                                                                echo "*";   
                                                            }
                                                        ?>
                                                    </td>
                                                    <!-- pajak -->
                                                    
                                                </tr>
                                                <?php $totalrek += $u->nominal ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    <?php } ?>
                                    <tr>
                                        <th colspan="3">Jumlah</th>
                                        <th><?= number_format($total)?></th>
                                        <th><?= number_format($totalrek) ?></th>
                                        <th><?= number_format($total - $totalrek) ?></th>
                                        <th colspan="3">-</th>
                                        <th><?= number_format($pph215) ?></th>
                                        <th><?= number_format($pph216) ?></th>
                                        <th><?= number_format($pph217) ?></th>
                                        <th><?= number_format($pph2178) ?></th>
                                        <th><?= number_format($pph221) ?></th>
                                        <th><?= number_format($pph223) ?></th>
                                        <th><?= number_format($pph232) ?></th>
                                        <th><?= number_format($pph234) ?></th>
                                        <th><?= number_format($ppn) ?></th>
                                        <th><?= number_format($final05) ?></th>
                                        <th><?= number_format($final) ?></th>
                                        <th><?= number_format($sspd) ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
       
        <script type="text/javascript">
            $('.preview').click(function(e) {
                window.print();
            })

            $(document).ready(function() {
               window.print();
            });
        </script>
    </body>
</html>