<?php  

	class PanjarRekeningModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_panjar_rekening' ;
	    }

	    ## get all data in table
	    function getAll($id) {

	    	$this->db->select('data_panjar_rekening.*,
	    		m_rekening.kode as kode, m_rekening.description as description');

	    	$this->db->join('m_rekening', 'm_rekening.id = data_panjar_rekening.id_rekening', 'left'); 

	    	$this->db->where('data_panjar_rekening.is_active','1');
	    	$this->db->where('data_panjar_rekening.id_kegiatan',$id);

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}


		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_panjar_rekening.id, data_panjar_rekening.name');

	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	    	
	        $this->db->where(array('data_panjar_rekening.id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        $this->db->insert($this->table_name, $a_input);
	        
	        return $this->db->error();	   
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		## get data by id in table
	    function getAllByID($id) {

	    	$this->db->select('
	    		data_panjar_rekening.*,
	    		m_rekening.kode as koderekening, 
	    		m_rekening.description as descriptionrekening'
	    	);

	    	$this->db->join('m_rekening', 'm_rekening.id = data_panjar_rekening.id_rekening', 'left'); 

	    	$this->db->where(array('data_panjar_rekening.is_active' => '1'));
	        $this->db->where(array('data_panjar_rekening.id_kegiatan' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->result();
	    }

	    function getPengajuanByID($id,$bulan,$tahun) {

	    	$this->db->select('
	    		data_panjar_rekening.*,
	    		m_rekening.kode as koderekening, 
	    		m_rekening.description as descriptionrekening'
	    	);

	    	$this->db->join('m_rekening', 'm_rekening.id = data_panjar_rekening.id_rekening', 'left'); 

	    	$this->db->where(
	    		array(
	    			'data_panjar_rekening.is_active' => '1',
	    			'data_panjar_rekening.id_kegiatan' => $id,
	    			'YEAR(data_panjar_rekening.date_created)' => $tahun,
	    			'MONTH(data_panjar_rekening.date_created)' => $bulan
	    		)
	    	);
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->result();
	    }
	}

?>