<?php  

	class DetailRekeningModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_panjar_rekening_realisasi' ;
	    }

	    ## get all data in table
	    function getAll($id) {	    	
	    	$this->db->where(
	    		array(
	    			'data_panjar_rekening_realisasi.id_panjar_rekening' => $id[0] ,
	    			'data_panjar_rekening_realisasi.id_rekening' => $id[1] ,
	    			'data_panjar_rekening_realisasi.is_active' => 1 ,
	    		)
	    	);

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}


		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_panjar_rekening.id, data_panjar_rekening.name');

	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	    	
	        $this->db->where(array('data_panjar_rekening_realisasi.id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }


	    function getDetail($id) {

	    	$this->db->select('
	    		data_panjar.name, 
	    		data_panjar.tanggal, 
	    		m_department.name as id_department, 
	    		m_kegiatan.kode as kode, m_kegiatan.description as description,
	    		m_rekening.id as id_rekening, m_rekening.kode as koderekening, m_rekening.description as descriptionrekening,'
	    	);

	    	$this->db->join('data_panjar', 'data_panjar.id = data_panjar_rekening.id_kegiatan', 'left'); 
	    	$this->db->join('m_rekening', 'm_rekening.id = data_panjar_rekening.id_rekening', 'left'); 
	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  

	        $this->db->where(array('data_panjar_rekening.id' => $id));

	        $query = $this->db->get('data_panjar_rekening');
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        if($a_input['pph21'] == '99') {
	        	$a_input['pph21'] = $a_input['custominput'];
	        }

	        $a_input['id_panjar_rekening'] 	 = $a_input['id_parent'] ;
	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        unset($a_input['id_parent']);	
	        unset($a_input['custominput']);
	        
	        $this->db->insert($this->table_name, $a_input);
	        
	        return $this->db->error();	   
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['id_panjar_rekening'] 	 = $a_input['id_parent'] ;
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        if($a_input['pph21'] == '99') {
	        	$a_input['pph21'] = $a_input['custominput'];
	        }

	        unset($a_input['id_parent']);
	        unset($a_input['custominput']);

	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		function getAllByID($id_panjar_rekening,$id_rekening) {
	    	
	        $this->db->where(
	        	array(
	        		'data_panjar_rekening_realisasi.is_active' => '1',
	        		'data_panjar_rekening_realisasi.id_panjar_rekening' => $id_panjar_rekening,
	        		'data_panjar_rekening_realisasi.id_rekening' => $id_rekening
	        	)
	        );
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->result();
	    }


	    function getByIDTransaksi($id_transaksi) {
	    	
	        $this->db->where(array('data_panjar_rekening_realisasi.id_transaksi' => $id_transaksi));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    function getDepartment($id) {
	    	$this->db->select('
	    		m_department.name as id_department'
	    		
	    	);

	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  

	        $this->db->where(array('data_panjar.id' => $id));

	        $query = $this->db->get('data_panjar');
	        
	        return $query->row();
	    }
	}

?>