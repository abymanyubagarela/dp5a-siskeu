<?php  

	class PanjarModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_panjar' ;
	    }

	    ## get all data in table
	    function getAll() {
	    	$this->db->select('data_panjar.id, data_panjar.name, 
	    		data_panjar.tanggal, data_panjar.date_created,data_panjar.jenis,
	    		m_department.name as id_department, 
	    		m_kegiatan.kode as kode, m_kegiatan.description as description');

	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  
	    	$this->db->order_by('data_panjar.date_created', 'desc');
	    	$this->db->where('data_panjar.is_active','1');

	    	if($this->session->userdata['auth']->id_role != '1') {
				$this->db->where('m_kegiatan.id_department',$this->session->userdata['auth']->id_department);	    		
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table year
	    function getAllbydate($tahun,$bulan) {
	    	$this->db->select('data_panjar.id, data_panjar.name, 
	    		data_panjar.tanggal, data_panjar.date_created,, data_panjar.jenis,
	    		m_department.name as id_department, 
	    		m_kegiatan.kode as kode, m_kegiatan.description as description');

	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  
	    	$this->db->order_by('data_panjar.date_created', 'desc');
	    	$this->db->where('data_panjar.is_active','1');

	    	if($this->session->userdata['auth']->id_role != '1') {
				$this->db->where('m_kegiatan.id_department',$this->session->userdata['auth']->id_department);	    		
	    	}

	    	if(!empty($bulan) && !empty($tahun)) {
	    		// $this->db->where('data_panjar.tanggal >=', $tahun.'-'.$bulan.'-01');
	    		// $this->db->where('data_panjar.tanggal <=', $tahun.'-'.$bulan.'-31');
	    		$this->db->where('data_panjar.tanggal BETWEEN "'. date('Y-m-d', strtotime($tahun.'-'.$bulan.'-01')). '" and "'. date('Y-m-d', strtotime($tahun.'-'.$bulan.'-31')).'"');
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}


		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_panjar.id, data_panjar.name');

	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	    	$this->db->select('data_panjar.id, data_panjar.name, 
	    		data_panjar.tanggal, data_panjar.jenis, 
	    		m_department.name as id_department, 
	    		m_kegiatan.kode as kode, m_kegiatan.description as description');

	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  
	    	
	        $this->db->where(array('data_panjar.id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        $this->db->insert($this->table_name, $a_input);
	        
	        return $this->db->error();	   
	    }

	    ## update data in table
	    function isKegiatanExist($id_kegiatan) {
	        $this->db->where('id_kegiatan', $id_kegiatan);

	        $this->db->where(
	        	array(
	        		'id_kegiatan' => $id_kegiatan,
	        		'date_created >=' => date('Y-m-01', strtotime('now')),
	        		'date_created <=' => date('Y-m-t', strtotime('now')),
	        	)
	        );

	        $query = $this->db->get($this->table_name);

	        return $query->result();

	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		## get data dashboard
	    function getDashboard($id) {
	        $this->db->select('data_panjar.id');

	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_panjar.id_kegiatan', 'left'); 
	    	$this->db->join('m_department', 'm_department.id = m_kegiatan.id_department', 'left');  
	    	
	        $this->db->where(
	        	array(
	        		'm_kegiatan.id_department' => $id,
	        		'data_panjar.tanggal >=' => date('Y-m-01', strtotime('now')),
	        		'data_panjar.tanggal <=' => date('Y-m-t', strtotime('now')),
	        		'data_panjar.is_active' => 1 
	        	)
	        );
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get all data in table for list (pengajuan)
	    function getListPengajuan($tahun,$bulan) {
	    	
	    	$sql = "SELECT data_panjar.id ,data_panjar.jenis, m_kegiatan.description , m_kegiatan.kode , m_department.name FROM data_panjar 
	    			LEFT JOIN m_kegiatan ON m_kegiatan.id = data_panjar.id_kegiatan
	    			LEFT JOIN m_department ON m_department.id = m_kegiatan.id_department
	    			WHERE YEAR(data_panjar.date_created) = ".$tahun."
	    			AND MONTH(data_panjar.date_created) = ".$bulan."
	    			AND data_panjar.is_active = 1 order by name";

	        $query = $this->db->query($sql);

	        return $query->result();
		}

		## get total detail panjar (nominal)
		function sumPanjar($id) {
	        $this->db->select('sum(data_panjar_rekening.nominal) as total');

	    	$this->db->where(
	        	array(
	        		'data_panjar_rekening.id_kegiatan' => $id ,
	        		'data_panjar_rekening.is_active' => 1 
	        	)
	        );
	        
	        $query = $this->db->get('data_panjar_rekening');
	        
	        return $query->row()->total;
	    } 

	}

?>