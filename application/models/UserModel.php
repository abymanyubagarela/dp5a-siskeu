<?php  

	class UserModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_user' ;
	    }

	    ## get all data in table
	    function getAll() {
	    	
	    	$this->db->select('data_user.*, m_role.name as id_role, m_department.name as id_department');
	    	$this->db->join('m_role', 'm_role.id = data_user.id_role', 'left');  
	    	$this->db->join('m_department', 'm_department.id = data_user.id_department', 'left');  
	    	$this->db->where(
	    		array(
	    			'data_user.is_active' => '1',
	    			'data_user.id_role !=' => '1'
	    		)
	    	);

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_user.id, data_user.name');
	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get data by id_role in table
	    function getByRole($id) {
	    	$this->db->select('data_user.id, data_user.name');
	        $this->db->where(array('id_role' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->result();
	    }

	    ## get data by name in table
	    function getByName($id) {
	    	$this->db->select('data_user.name');
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        unset($a_input['type']);
	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        unset($a_input['type']);
	        
	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		function getLogin() {

			$_data = $this->input->post() ;

			foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $this->db->where(
	    		array(
	    			'data_user.email' 	  => $a_input['email'],
	    			'data_user.password'  => $a_input['password'],
	    			'data_user.is_active' => 1
	    		)
	    	);	   

	        $query = $this->db->get($this->table_name);
				        
	        return $query->row();
	    }
	}
?>